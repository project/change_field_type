<?php

namespace Drupal\change_field_type\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Change Field Type Form.
 */
class ChangeFieldTypeForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->database = $container->get('database');

    return $instance;
  }

  /**
   * Add decimal settings.
   *
   * @param array $form
   *   Form element.
   */
  public function addDecimalSettingsElements(array &$form) {
    $form['decimal_settings'] = [
      '#type' => 'details',
      '#title' => 'Decimal settings',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="type_to_change"]' => ['value' => 'decimal'],
        ],
      ],
    ];
    $form['decimal_settings']['precision'] = [
      '#title' => $this->t('Precision'),
      '#type' => 'number',
      '#required' => TRUE,
      '#default_value' => 20,
    ];
    $form['decimal_settings']['scale'] = [
      '#title' => $this->t('Scale'),
      '#type' => 'number',
      '#required' => TRUE,
      '#default_value' => 2,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'change_field_type.changefieldtype',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'change_field_type_form';
  }

  /**
   * Get default settings of field type.
   *
   * @param mixed $type
   *   Type of field.
   *
   * @return array|mixed
   *   Settings.
   */
  public static function getDefaultSettings($type = NULL) {
    $settings = [
      'integer' => [
        'unsigned' => FALSE,
        'size' => 'normal',
      ],
      'decimal' => [
        'precision' => 20,
        'scale' => 2,
      ],
      'float' => [],
    ];
    if (empty($type)) {
      return $settings;
    }
    return $settings[$type];
  }

  /**
   * Get type to change.
   *
   * @return array|mixed
   *   Settings.
   */
  public function getTypeToChange() {
    return [
      'float' => [
        'decimal' => 'decimal',
        'integer' => 'integer',
      ],
      'decimal' => [
        'float' => 'float',
        'integer' => 'integer',
      ],
      'integer' => [
        'decimal' => 'decimal',
        'float' => 'float',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('change_field_type.changefieldtype');

    //    $view_storage = $this->entityTypeManager->getStorage('view');
    //    $views = $view_storage->load('geolocation_demo_commonmap_with_marker_icons');
    //    dump($views->get('display'));
    //    $this->getRelatedView(FieldStorageConfig::load('node.field_base_premium'));

    $form['#id'] = $this->getFormId();
    $msg = $this->t('This action will affect your data. Please backup the database before changing.');
    $form['warning'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['messages', 'messages--error'],
      ],
      'message' => [
        '#markup' => '<div>' . $msg . '</div>',
      ],
    ];

    $field_storage = $this->entityTypeManager->getStorage('field_storage_config');
    $query = $field_storage->getQuery();
    $query->condition('type', ['decimal', 'float', 'integer'], 'IN');
    $query->condition('entity_type', 'node');
    $results = $query->execute();
    $fields = $field_storage->loadMultiple($results);
    $options = [
      '' => '- none -',
    ];
    /** @var \Drupal\field\Entity\FieldStorageConfig $field */
    foreach ($fields as $id => $field) {
      $options[$id] = $field->getName() . ' - ' . $field->getType();
    }

    $form['field'] = [
      '#title' => $this->t('Select field'),
      '#type' => 'select',
      '#options' => $options,
      '#ajax' => [
        'wrapper' => $this->getFormId(),
        'callback' => [$this, 'ajaxSelectField'],
      ],
      '#required' => TRUE,
    ];
    if (!empty($form_state->getValue('field'))) {
      $field_selected = FieldStorageConfig::load($form_state->getValue('field'));
      $form['selected_field'] = [
        '#type' => 'table',
        '#header' => [$this->t('Field summary'), ''],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Name'],
        'value' => ['#plain_text' => $field_selected->getName()],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Type'],
        'value' => ['#plain_text' => $field_selected->getType()],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Cardinality'],
        'value' => ['#plain_text' => $field_selected->getCardinality()],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Entity type'],
        'value' => ['#plain_text' => $field_selected->get('entity_type')],
      ];
      $field_storage = FieldStorageConfig::loadByName('node', $field_selected->getName());
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Bundles'],
        'value' => [
          '#plain_text' => implode(', ', $field_storage->getBundles()),
        ],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Table'],
        'value' => ['#markup' => $this->getTableSummary($field_selected)],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Max value'],
        'value' => ['#markup' => $this->getTableMaxNumber($field_selected)],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => 'Table Revision'],
        'value' => ['#markup' => $this->getRevisionTableSummary($field_selected)],
      ];
      $form['selected_field'][] = [
        'name' => ['#plain_text' => $this->t('Using in views')],
        'value' => [
          '#markup' => '<em>' . $this->t('*This list maybe missing') . '</em><br/>'
            . $this->getRelatedView($field_selected),
        ],
      ];
      $type_to_change = $this->getTypeToChange();
      $type_to_change = $type_to_change[$field_selected->getType()];
      $type_to_change = ['' => '- none -'] + $type_to_change;
      $form['type_to_change'] = [
        '#title' => $this->t('Select type'),
        '#type' => 'select',
        '#options' => $type_to_change,
        '#required' => TRUE,
      ];
      $this->addDecimalSettingsElements($form);
      $this->addIntSettingsElements($form);

      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Change field type'),
        '#states' => [
          'enabled' => [
            ':input[name="type_to_change"]' => ['!value' => ''],
          ],
        ],
      ];
      $form['actions']['message'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['messages', 'messages--error'],
        ],
        'message' => [
          '#markup' => '<div>' . $msg . '</div>',
        ],
        '#states' => [
          'visible' => [
            ':input[name="type_to_change"]' => ['!value' => ''],
          ],
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $selected_field = $form_state->getValue('field');
    $type_to_change = $form_state->getValue('type_to_change');
    $field = FieldStorageConfig::load($selected_field);

    $data['entity_type'] = 'node';
    $data['field_name'] = $field->getName();
    $data['table'] = 'node__' . $data['field_name'];
    $data['revision_table'] = 'node_revision__' . $data['field_name'];
    $data['type_to_change'] = $type_to_change;
    $data['settings'] = [];

    $data['settings']['precision'] = $form_state->getValue('precision');
    $data['settings']['scale'] = $form_state->getValue('scale');

    $data['backup_string'] = uniqid('_backup_');
    $data['table_backup'] = 'cft' . $data['backup_string'];
    $data['revision_table_backup'] = 'cft_revision' . $data['backup_string'];

    $operations = [
      [
        [self::class, 'processBackupDataTable'],
        [$data],
      ],
      [
        [self::class, 'processDeleteCreateField'],
        [$data],
      ],
      [
        [self::class, 'processImportData'],
        [$data],
      ],
    ];
    $batch = [
      'title' => $this->t('Processing to change field type...'),
      'operations' => $operations,
      'finished' => [self::class, 'finishBatch'],
    ];
    batch_set($batch);
  }

  /**
   * Create backup tables.
   *
   * @param array $data
   *   Data of field.
   * @param array $context
   *   Context of batch.
   */
  public static function processBackupDataTable(array $data, array &$context) {
    $database = \Drupal::database();
    $table = $data['table'];
    $revision_table = $data['revision_table'];
    $table_backup = $data['table_backup'];
    $revision_table_backup = $data['revision_table_backup'];

    $database->query('CREATE TABLE ' . $table_backup . ' AS SELECT * FROM ' . $table . ';');
    $database->query('CREATE TABLE ' . $revision_table_backup . ' AS SELECT * FROM ' . $revision_table . ';');

    $context['message'] = 'Created backup tables';
  }

  /**
   * Import data from backup.
   *
   * @param array $data
   *   Data of field.
   * @param array $context
   *   Context of batch.
   */
  public static function processImportData(array $data, array &$context) {
    $database = \Drupal::database();
    $table = $data['table'];
    $revision_table = $data['revision_table'];
    $table_backup = $data['table_backup'];
    $revision_table_backup = $data['revision_table_backup'];

    $database->query('INSERT IGNORE INTO ' . $table . ' SELECT * FROM ' . $table_backup . ';');
    $database->query('INSERT IGNORE INTO ' . $revision_table . ' SELECT * FROM ' . $revision_table_backup . ';');

    $context['message'] = 'Imported data from backup tables';
  }

  /**
   * Process field type.
   *
   * @param array $data
   *   Data of field.
   * @param array $context
   *   Context of batch.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processDeleteCreateField(array $data, array &$context) {
    $entity_type = $data['entity_type'];
    $field_name = $data['field_name'];
    $type_to_change = $data['type_to_change'];

    // Step 1: Get field storage.
    $field_storage = FieldStorageConfig::loadByName($entity_type, $field_name);

    // Check if field not found.
    if (is_null($field_storage)) {
      return;
    }

    // Step 3: Save new field configs & delete existing fields.
    $new_fields = [];
    foreach ($field_storage->getBundles() as $bundle => $label) {
      /** @var \Drupal\field\Entity\FieldConfig $field */
      $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      $new_field = $field->toArray();
      $new_field['field_type'] = $type_to_change;
      $new_fields[] = $new_field;
      // Delete field.
      $field->delete();
    }

    $new_field_storage = $field_storage->toArray();
    $new_field_storage['type'] = $type_to_change;
    $settings = self::getDefaultSettings($type_to_change);
    if ('decimal' == $type_to_change) {
      $settings['precision'] = $data['settings']['precision'];
      $settings['scale'] = $data['settings']['scale'];
    }

    $new_field_storage['settings'] = $settings;

    field_purge_batch(250);

    FieldStorageConfig::create($new_field_storage)->save();

    foreach ($new_fields as $new_field) {
      $new_field = FieldConfig::create($new_field);
      $new_field->save();
    }

    $context['message'] = 'Processing to change field type';
  }

  /**
   * Finish Batch.
   *
   * @param mixed $success
   *   Success.
   * @param mixed $results
   *   Results.
   * @param mixed $operations
   *   Operations.
   */
  public static function finishBatch($success, $results, $operations) {
    if ($success) {
      $message = t('Finished. Please check your Form Display to enable the field.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

  /**
   * Ajax select field callback.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal\Core\Form\FormStateInterface.
   *
   * @return array
   *   Form array.
   */
  public function ajaxSelectField(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Get table summary.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $field_selected
   *   Drupal\field\Entity\FieldStorageConfig.
   *
   * @return string
   *   Summary string.
   */
  private function getTableSummary(FieldStorageConfig $field_selected) {
    $table = 'node__' . $field_selected->getName();
    $select = $this->database->select($table, $table);
    $select->fields($table, []);
    $result = $select->countQuery()->execute()->fetchField();
    return $table . '<br/>' . $this->t('Number of records: %record', ['%record' => $result]);
  }

  /**
   * Get revision table summary.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $field_selected
   *   Drupal\field\Entity\FieldStorageConfig.
   *
   * @return string
   *   Summary string.
   */
  private function getRevisionTableSummary(FieldStorageConfig $field_selected) {
    $table = 'node_revision__' . $field_selected->getName();
    $select = $this->database->select($table, $table);
    $select->fields($table, []);
    $result = $select->countQuery()->execute()->fetchField();
    return $table . '<br/>' . $this->t('Number of records: %record', ['%record' => $result]);
  }

  /**
   * Get Max number.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $field_selected
   *   Drupal\field\Entity\FieldStorageConfig.
   *
   * @return string
   *   String data.
   */
  private function getTableMaxNumber(FieldStorageConfig $field_selected) {
    $table = 'node__' . $field_selected->getName();
    $select = $this->database->select($table, $table);
    $select->fields($table, [$field_selected->getName() . '_value']);
    $select->range(0, 1);
    $select->orderBy($field_selected->getName() . '_value', 'desc');
    $result = $select->execute()->fetchField();
    if (empty($result)) {
      return '';
    }
    $text = [];
    $text[] = $result;
    $text[] = '<b>Len: ' . strlen($result) . '</b>';
    return implode('<br/>', $text);
  }

  /**
   * Add int settings elements.
   *
   * @param array $form
   *   Form.
   */
  private function addIntSettingsElements(array &$form) {
    $form['integer_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Integer settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="type_to_change"]' => ['value' => 'integer'],
        ],
      ],
    ];
    $form['integer_settings']['message'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'color:red;',
      ],
      'text' => [
        '#markup' => $this->t('Your data will be lost if the values larger than INT data type.'),
      ],
    ];
  }

  /**
   * Find related view.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $field_selected
   *   Drupal\field\Entity\FieldStorageConfig.
   *
   * @return string
   *   Output.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getRelatedView(FieldStorageConfig $field_selected) {
    $table = 'node__' . $field_selected->getName();
    $query = $this->database->query("SELECT `name` FROM `config` WHERE `name` LIKE 'views.%' and `data` LIKE '%:field%'", [':field' => $table]);
    $result = $query->fetchAllAssoc('name');
    if (empty($result)) {
      return '';
    }
    $view_storage = $this->entityTypeManager->getStorage('view');
    $result = array_keys($result);
    $output = [];
    foreach ($result as $name) {
      $id = str_replace('views.view.', '', $name);
      $view = $view_storage->load($id);
      foreach ($view->get('display') as $key => $display) {
        $encode = Json::encode($display);
        if (FALSE !== strpos($encode, $table)) {
          $edit_url = Url::fromUri('internal:/admin/structure/views/view/' . $id . '/edit/' . $display['id'])
            ->toString();
          $output[] = 'View: ' . $id . ' - Display: ' . $display['id'] . ' - <a target="_blank" href="' . $edit_url . '">Edit</a>';
        }
      }
    }

    return implode('<br/>', $output);
  }

}
